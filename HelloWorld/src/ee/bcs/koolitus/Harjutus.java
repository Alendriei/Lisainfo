package ee.bcs.koolitus;

public class Harjutus {

	public static void main(String[] args) {
		byte a = 1;
		byte b = 1;
		byte c = 3;
		a=c;
		System.out.println (a==b);
		System.out.println (a==c);
		
		byte x1 = 10;
		byte x2 = 20;
		byte y1 = ++x1;
		System.out.println("x1 = " + x1 + " y1 = " + y1);
		byte y2 = x2++;
		System.out.println("x2 = " + x2 + " y2 = " + y2);
	}

}
