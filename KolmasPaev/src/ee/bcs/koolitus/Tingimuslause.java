package ee.bcs.koolitus;

public class Tingimuslause {

	public static void main(String[] args) {
		String isikukood = "587289403871";
		int esimene = Integer.valueOf(isikukood.substring(0, 1));
		System.out.println(esimene);
		if (esimene == 4) {
			System.out.println("naine");
		}
		if (esimene % 2 == 0) {
			System.out.println("naine");
		} else if (esimene % 2 == 1) {
			System.out.println("mees");
		}
		String sugu = esimene % 2 == 0 ? "naine" : "mees";
		System.out.println("Sugu = " + sugu);
	}

}
