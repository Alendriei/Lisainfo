package ee.bcs.koolitus;

public class Stringid {

	public static void main(String[] args) {
		/*/int i = 7; byte b = i;  // translaator annab vea​
		byte b = 7; int i = b;  // viga ei ole​
		// arve saab teisendada pikemast lühemasse tüüpi​
		Integer i = 7; byte b = i.byteValue(); // OK​
		Integer i = 777; byte b = i.byteValue(); // = 9 ​
		// 777 = 0x0309 – byteValue võtab viimase baidi​
		int i = 7; byte b = i.byteValue(); // viga​
		// int pole klass ja tal pole meetodeid​
		int i = 7; byte bx = ((Integer)ix).byteValue();​
		// siin on kasutatud tüübiteisenduse tehet casting
		
		String neli = "44"; // see on tekst​
		int iS = Integer.parseInt(neli); // OK​
		//i = Integer.parseInt("tere"); // viga täitmisel​
		String vastus1 = ((Integer)iS).toString(); // viga -  pole klass​
		//String vastus = ((Integer)i).toString(); // OK​
		System.out.println(vastus1);​
		// klassidel on enamasti meetod, kuidas teksti sellesse​
		// klassi teisendada (parseX) ja kuidas klassi​
		// stringiks teisendada (toString)
		 * */
		String NimedeLoeteluTekst = "See on loetelu linnade nimedest: Tallinn, Tartu, Pärnu, Narva";
		String[] splitKooloniga = NimedeLoeteluTekst.split(":");
		System.out.println("SplitKooloniga massiivis on " + splitKooloniga.length + "elementi");
		System.out.println("Esimene element on:\"" + splitKooloniga[0] + "\"");
		System.out.println("Teine element on :\"" + splitKooloniga[1].trim() + "\"");
		String[] splitKomaga = splitKooloniga[1].split(",");
		System.out.println(splitKomaga[0]);
		System.out.println(splitKomaga[1]);
		System.out.println(splitKomaga[2]);
		System.out.println(splitKomaga[3]);
		
		for (String nimi : splitKomaga) {
			System.out.println(nimi);
		}
	}

}
