package ee.bcs.koolitus;

public class BrokenClass {
	boolean isValid = false;
	String s = new String("This class has many bugs");
	public int niceNumber = 1_000_000;
	public double anotherNiceNumber = 1000.00;
	String s2 = "Is there more bugs?";

	public BrokenClass(){
		System.out.println("All bugs are fixed");
	}

	public static void main(String args[]) {
		BrokenClass bc = new BrokenClass();
	}
}
